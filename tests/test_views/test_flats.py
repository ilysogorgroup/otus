from time import time

from pytest import fixture
from flask import url_for

from app import app
from models.flats import Flats


@fixture
def client():
    with app.test_client() as client:
        with app.app_context():
            yield client


def test_add_product(client):
    url_add = url_for("flat_app.add")
    name = f"test_flat_{time()}"
    data = {
        "name": name,
        "address": "Test address",
        "electricity_t1": 1,
        "hot_water": 1,
        "cold_water": 1
    }
    resp = client.post(url_add, data=data,
                       mimetype="application/x-www-form-urlencoded")

    assert resp.status_code < 400

    flat = Flats.query.filter_by(name=name).one()
    assert flat.address == "Test address"
